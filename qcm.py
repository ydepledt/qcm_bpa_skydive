# -*- coding: utf-8 -*-

import random
import re

# Constants
NUMBER_OF_QUESTIONS = 463  # The total number of questions available

# Initialize variables
score = 0
questions = []
reponses = []

# Function to check if a line contains a number
def testNumber(line):
    for element in listOfNumber:
        if element in line:
            return True
    return False

# Function to check if a line contains a letter
def testLetter(line):
    for element in listOfLetter:
        if element in line:
            return True
    return False

# Read questions from "qcm_questions.txt" file
with open("qcm_questions.txt", "r", encoding="utf-8") as file:
    lignes = file.readlines()
    index = -1
    listOfNumber = [str(i) + '.' for i in range(10)]
    listOfLetter = [chr(ord('A') + i) + '.' for i in range(4)]

    for i in range(len(lignes)):
        if testNumber(lignes[i][:3]) and not testLetter(lignes[i][:3]):
            # Use regular expression to extract text after the number
            question_text = re.sub(r'^\d+\.\s*', '', lignes[i])
            questions.append(question_text)
            index += 1
        elif testNumber(lignes[i][:3]) and testLetter(lignes[i][:3]):
            questions[index] += lignes[i]
        elif not testNumber(lignes[i][:3]) and testLetter(lignes[i][:3]):
            questions[index] += lignes[i]
        else:
            questions[index] += lignes[i]

# Remove newline characters from questions
questions = [question.strip() for question in questions]

# Read answers from "qcm_reponses.txt" file
with open("qcm_reponses.txt", "r", encoding="utf-8") as file_r:
    lignes = file_r.readlines()
    for line in lignes:
        reponses.append(line.strip()[-1])  # Extract the last character as the answer

# Get the number of questions the user wants
nombreDeQuestions = 0
while nombreDeQuestions <= 0 or nombreDeQuestions > len(questions):
    nombreDeQuestions = int(input("Choisissez le nombre de questions : "))
    if nombreDeQuestions <= 0 or nombreDeQuestions > len(questions):
        print("Choisissez un nombre entre 1 et", len(questions))

# Select random questions
random_questions = random.sample(range(len(questions)), nombreDeQuestions)

# Initialize count for keeping track of the current question
count = 0

# Loop through random questions
for i in random_questions:
    count += 1
    print("\n\nQuestion", count, ":", questions[i])
    user_response = input("Réponse (A, B, C, D) : ").upper()  # Convert input to uppercase

    # Check if the user's response is correct
    if user_response == reponses[i]:
        score += 1
        print("Correct!")
    else:
        print("Faux, la réponse est :", reponses[i])

    if count != nombreDeQuestions:
        print("Score :", score, "/", count)

# Display the final score
print("\nVotre score est :", score, "/", nombreDeQuestions)

